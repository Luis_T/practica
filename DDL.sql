CREATE DATABASE `db_empresa`;

USE `db_empresa`;

CREATE TABLE `genders` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `jobs` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `salary` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `employees` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `birth_date` date DEFAULT NULL,
  `gender_id` bigint DEFAULT NULL,
  `job_id` bigint DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2ml2wdxfngkqqur3k4bgo0gdj` (`gender_id`),
  KEY `FKnpqyu6u0fdh2rmqtoue23gxb4` (`job_id`),
  CONSTRAINT `FK2ml2wdxfngkqqur3k4bgo0gdj` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`),
  CONSTRAINT `FKnpqyu6u0fdh2rmqtoue23gxb4` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `employee_worked_hours` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `employee_id` bigint DEFAULT NULL,
  `worked_date` date DEFAULT NULL,
  `worked_hours` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKhoth0hiwrrgrc31qjy6axownd` (`employee_id`),
  CONSTRAINT `FKhoth0hiwrrgrc31qjy6axownd` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


