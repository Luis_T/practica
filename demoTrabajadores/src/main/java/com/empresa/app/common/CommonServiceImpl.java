package com.empresa.app.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

//La "E" es la entity que se va a guardar en la base de datos.
//La "R" es la interfaz repository que vamos a parametrizar, Del lado derecho es el padre de "R" y crudRepository necesitamos indicarle la "E" entidad a persistir
// y el tipo del indice "Long" de la entidad.
public class CommonServiceImpl<E, R extends CrudRepository<E, Long>> implements CommonService<E>{

    @Autowired
    protected R repository;

    @Override
    public Iterable<E> findAll() {
        return this.repository.findAll();
    }

    @Override
    public Optional<E> findById(Long id) {
        return this.repository.findById(id);
    }

    @Override
    public E save(E entity) {
        return this.repository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        this.repository.deleteById(id);
    }
}
