package com.empresa.app.common;

import java.util.Optional;

//Genericos: se pueden sustituir las letras por tipos de clase o tipos de objetos o datos.
//
public interface CommonService <E>{
    Iterable<E> findAll();

    Optional<E> findById(Long id);

    E save(E entity);

    void deleteById(Long id);
}
