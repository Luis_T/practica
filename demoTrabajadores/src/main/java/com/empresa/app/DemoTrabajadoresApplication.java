package com.empresa.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoTrabajadoresApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoTrabajadoresApplication.class, args);
	}

}
