package com.empresa.app.employeedworkedhours.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class FindHoursTotalResponse {
    private List<String> errores;
    private Double totalWorkedHours;
    private Boolean success;
}
