package com.empresa.app.employeedworkedhours.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
public class FindHoursTotalRequest {
    private Long employeedId;
    private LocalDate startDate;
    private LocalDate finalDate;
}
