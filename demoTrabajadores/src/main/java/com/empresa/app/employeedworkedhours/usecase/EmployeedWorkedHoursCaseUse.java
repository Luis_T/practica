package com.empresa.app.employeedworkedhours.usecase;

import com.empresa.app.employeed.repository.EmployeeRepository;
import com.empresa.app.employeedworkedhours.model.EmployeedWorkedHoursResponse;
import com.empresa.app.employeedworkedhours.repository.EmployeedWorkedHoursRepository;
import com.empresa.app.employeedworkedhours.usecase.request.FindHoursTotalRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeedWorkedHoursCaseUse {
    private final EmployeedWorkedHoursRepository employeedWorkedHoursRepository;

    private final EmployeeRepository employeeRepository;

    public EmployeedWorkedHoursCaseUse(EmployeedWorkedHoursRepository employeedWorkedHoursRepository, EmployeeRepository employeeRepository) {
        this.employeedWorkedHoursRepository = employeedWorkedHoursRepository;
        this.employeeRepository = employeeRepository;
    }

    public EmployeedWorkedHoursResponse findHoursTotal(FindHoursTotalRequest request){
        Double hoursTotal = null;
        List<String> errores = new ArrayList<>();

        if (!this.employeeRepository.existEmployeed(request.getEmployeedId()))
            errores.add("El empleado con id: "+request.getEmployeedId()+" no existe");

        if (!request.getStartDate().isBefore(request.getFinalDate()))
            errores.add("La fecha de inicio no debe ser mayor a la fecha final");

        if (errores.size() == 0)
            hoursTotal = this.employeedWorkedHoursRepository.findEmployeedByDate(request.getEmployeedId(), request.getStartDate(), request.getFinalDate());
        return new EmployeedWorkedHoursResponse(hoursTotal, errores);
    }
}
