package com.empresa.app.employeedworkedhours.repository;

import java.time.LocalDate;

public interface EmployeedWorkedHoursRepository {
    Double findEmployeedByDate(Long employeedId, LocalDate startDate, LocalDate finalDate);

    Double findSalaryEmployee(Long employeedId, LocalDate startDate, LocalDate finalDate);
}
