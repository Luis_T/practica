package com.empresa.app.employeedworkedhours.repository;

import com.empresa.app.employeed.repositoryentity.EmployeeRepositoryEntity;
import com.empresa.app.employeedworkedhours.repositoryEntity.EmployeeWorkedHoursRepositoryEntity;
import com.empresa.app.entities.EmployeeWorkedHours;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class EmployeedWorkedHoursRepositoryImpl implements EmployeedWorkedHoursRepository{

    private final EmployeeWorkedHoursRepositoryEntity employeeWorkedHoursRepositoryEntity;

    private final EmployeeRepositoryEntity employeeRepositoryEntity;

    public EmployeedWorkedHoursRepositoryImpl(EmployeeWorkedHoursRepositoryEntity employeeWorkedHoursRepositoryEntity, EmployeeRepositoryEntity employeeRepositoryEntity) {
        this.employeeWorkedHoursRepositoryEntity = employeeWorkedHoursRepositoryEntity;
        this.employeeRepositoryEntity = employeeRepositoryEntity;
    }


    @Override
    public Double findEmployeedByDate(Long employeedId, LocalDate startDate, LocalDate finalDate) {
        List<EmployeeWorkedHours> employeeWorked = this.employeeWorkedHoursRepositoryEntity.findHoursByEmployeeForDate(employeedId, startDate, finalDate).get();
        return employeeWorked.stream().map(EmployeeWorkedHours::getWorkedHours).reduce(Double::sum).orElse(0.0);
    }

    @Override
    public Double findSalaryEmployee(Long employeedId, LocalDate startDate, LocalDate finalDate) {
        Double employeeSalary = this.employeeRepositoryEntity.findById(employeedId).get().getJobsId().getSalary();

        Double employeeHours = this.findEmployeedByDate(employeedId, startDate, finalDate);

        Double totalSalaryForEmployee = (employeeSalary * employeeHours);
        return totalSalaryForEmployee;
    }
}
