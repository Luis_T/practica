package com.empresa.app.employeedworkedhours.repositoryEntity;

import com.empresa.app.entities.EmployeeWorkedHours;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface EmployeeWorkedHoursRepositoryEntity extends JpaRepository<EmployeeWorkedHours, Long> {
    @Query(value = "SELECT * FROM employee_worked_hours e WHERE e.employee_id = :employeedId AND DATE (e.worked_date) BETWEEN :startDate AND :finalDate ORDER BY e.worked_date ASC", nativeQuery = true)
    Optional<List<EmployeeWorkedHours>> findHoursByEmployeeForDate(@Param("employeedId") Long employeedId,
                                                                   @Param("startDate") LocalDate startDate,
                                                                   @Param("finalDate") LocalDate finalDate);
}
