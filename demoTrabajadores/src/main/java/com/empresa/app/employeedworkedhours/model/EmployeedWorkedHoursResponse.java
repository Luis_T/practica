package com.empresa.app.employeedworkedhours.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class EmployeedWorkedHoursResponse {
    private Double total_worked_hours;
    private List<String> errores;
}
