package com.empresa.app.gender.model;

import lombok.Getter;

@Getter
public class SearcherGenderResponse {
    private Long id;
    private String name;

    public SearcherGenderResponse(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
