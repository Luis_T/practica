package com.empresa.app.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Getter
@Setter
@Entity
@Table(name = "jobs")
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "salary")
    private Double salary;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "jobsId")
    private List<Employees> employees;

    public Job(Long id) {
        this.id = id;
    }
}
