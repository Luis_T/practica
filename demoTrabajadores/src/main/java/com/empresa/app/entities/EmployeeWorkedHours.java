package com.empresa.app.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Getter
@Setter
@Entity
@Table(name = "employee_worked_hours")
public class EmployeeWorkedHours {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "employee_id")
    private Long employeedId;

    @Column(name = "worked_hours")
    private Double workedHours;

    @Column(name = "worked_date")
    private LocalDate workedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", insertable = false, updatable = false, nullable = false)
    private Employees employeesId;

    public EmployeeWorkedHours(Long id) {
        this.id = id;
    }
}
