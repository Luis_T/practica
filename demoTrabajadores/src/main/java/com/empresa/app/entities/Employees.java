package com.empresa.app.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Getter
@Setter
@Entity
@Table(name = "employees")
public class Employees {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "gender_id")
    private Long genderId;

    @Column(name = "job_id")
    private Long jobId;

    @Column(name = "name")
    private String name;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gender_id", insertable = false, updatable = false, nullable = false)
    private Genders gendersId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "job_id", insertable = false, updatable = false, nullable = false)
    private Job jobsId;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employeesId")
    private List<EmployeeWorkedHours> employeeWorkedHours;

    public Employees(Long id) {
        this.id = id;
    }
}
