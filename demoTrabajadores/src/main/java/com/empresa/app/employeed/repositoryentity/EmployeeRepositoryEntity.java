package com.empresa.app.employeed.repositoryentity;

import com.empresa.app.entities.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface EmployeeRepositoryEntity extends JpaRepository<Employees, Long> {
    @Query("SELECT E FROM Employees E WHERE E.name LIKE %:name%")
    Optional<Employees> findByName(String name);

    @Query("SELECT E FROM Employees E WHERE E.lastName LIKE %:lastName%")
    Optional<Employees> findByLastName(String lastName);
}
