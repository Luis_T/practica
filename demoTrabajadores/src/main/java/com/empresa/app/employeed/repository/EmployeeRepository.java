package com.empresa.app.employeed.repository;

import com.empresa.app.employeed.model.EmployeeModel;
import com.empresa.app.employeed.model.SearcherEmployeeResponse;

import java.util.List;

public interface EmployeeRepository {
    Boolean existName(String name);

    Boolean validLastName(String lastName);

    Long save(EmployeeModel model);

    List<SearcherEmployeeResponse> findAllEmployees();

    Boolean existEmployeed(Long employeedId);
}
