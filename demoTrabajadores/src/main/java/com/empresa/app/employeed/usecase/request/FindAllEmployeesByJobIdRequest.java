package com.empresa.app.employeed.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class FindAllEmployeesByJobIdRequest {
    private Long id;
}
