package com.empresa.app.employeed.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class FindSalaryEmployeeModel {
    private Double payment;
    private List<String> errores;
}
