package com.empresa.app.employeed.usecase;

import com.empresa.app.employeed.model.EmployeeModel;
import com.empresa.app.employeed.repository.EmployeeRepository;
import com.empresa.app.employeed.usecase.request.CreateEmployeeRequest;
import com.empresa.app.employeed.usecase.response.CreateEmployeeResponse;
import com.empresa.app.job.repository.JobRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

@Service
public class CreateEmployeeUseCase {

    private final EmployeeRepository employeedRepository;

    private final JobRepository jobRepository;

    public CreateEmployeeUseCase(EmployeeRepository employeedRepository, JobRepository jobRepository) {
        this.employeedRepository = employeedRepository;
        this.jobRepository = jobRepository;
    }

    public CreateEmployeeResponse execute(CreateEmployeeRequest request) {
        List<String> errores = new ArrayList<>();

        Long id = null;

        if (this.employeedRepository.existName(request.getName()))
            errores.add("Existe un registro con el nombre " + request.getName());

        if (this.employeedRepository.validLastName(request.getLastName()))
            errores.add("Existe un registro con el apellido " + request.getLastName());

        if (!this.jobRepository.exitsJob(request.getJobId()))
            errores.add("No existe el puesto con id: " + request.getJobId());

        if (!isOlder(request.getBirthDate()))
            errores.add("No es mayor de edad");

        EmployeeModel model = new EmployeeModel(request.getGenderId(), request.getJobId(), request.getName(), request.getLastName(), request.getBirthDate());
        if (errores.size() == 0)
            id = this.employeedRepository.save(model);

        return new CreateEmployeeResponse(id, errores);
    }

    private Boolean isOlder(LocalDate birthDate){
        Period periodo = Period.between(birthDate, LocalDate.now());

        return (periodo.getYears() >= 18);
    }

}
