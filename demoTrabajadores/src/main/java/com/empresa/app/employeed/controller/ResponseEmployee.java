package com.empresa.app.employeed.controller;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class ResponseEmployee {
    private Long id;
    private Boolean status;
    private List<String> errores;
}
