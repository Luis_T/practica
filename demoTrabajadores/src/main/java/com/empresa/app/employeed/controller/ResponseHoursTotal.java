package com.empresa.app.employeed.controller;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ResponseHoursTotal {
    private Double totalWorkedHours;
    private Boolean success;
}
