package com.empresa.app.employeed.controller;

import com.empresa.app.employeed.model.FindSalaryEmployeeModel;
import com.empresa.app.employeed.usecase.CreateEmployeeUseCase;
import com.empresa.app.employeed.usecase.FindSalaryEmployeeUseCase;
import com.empresa.app.employeed.usecase.request.CreateEmployeeRequest;
import com.empresa.app.employeed.usecase.request.FindSalaryEmployeeRequest;
import com.empresa.app.employeed.usecase.response.CreateEmployeeResponse;
import com.empresa.app.employeedworkedhours.model.EmployeedWorkedHoursResponse;
import com.empresa.app.employeedworkedhours.usecase.EmployeedWorkedHoursCaseUse;
import com.empresa.app.employeedworkedhours.usecase.request.FindHoursTotalRequest;
import com.empresa.app.employeedworkedhours.usecase.response.FindHoursTotalResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
public class EmployeeController {
    @Autowired
    private CreateEmployeeUseCase useCase;

    @Autowired
    private EmployeedWorkedHoursCaseUse workedHoursCaseUse;

    @Autowired
    private FindSalaryEmployeeUseCase findSalaryEmployeeUseCase;

    @PostMapping("/employees")
    public ResponseEntity<?> createEmployee(@RequestBody CreateEmployeeHttpRequest request){
        CreateEmployeeResponse reponse = this.useCase.execute(new CreateEmployeeRequest(
                request.getGenderId(),
                request.getJobId(),
                request.getName(),
                request.getLastName(),
                request.getBirthDate()
        ));
        if (reponse.getErrores().size() > 0)
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new ResponseEmployee(reponse.getId(),false, reponse.getErrores()));
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseEmployee(reponse.getId(),true, reponse.getErrores()));
    }

    @GetMapping(value = "/employees/{employeeId}/hours")
    public ResponseEntity<?> findTotalHours(@PathVariable("employeeId") Long employeeId,
                                            @RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                            @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate){
        EmployeedWorkedHoursResponse totalHours = this.workedHoursCaseUse.findHoursTotal(new FindHoursTotalRequest(employeeId, startDate, endDate));
        if (totalHours.getErrores().size() == 0)
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ResponseHoursTotal(totalHours.getTotal_worked_hours(), true));
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new ResponseHoursTotal(totalHours.getTotal_worked_hours(), false));
    }

    @GetMapping(value = "/employees/{employeeId}/salary")
    public ResponseEntity<?> findSalaryEmployee(@PathVariable("employeeId") Long employeeId,
                                            @RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                            @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate){
        FindSalaryEmployeeModel totalSalary = this.findSalaryEmployeeUseCase.findSalaryEmployee(new FindSalaryEmployeeRequest(employeeId, startDate, endDate));
        if (totalSalary.getErrores().size() == 0)
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ResponseHoursTotal(totalSalary.getPayment(), true));
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new ResponseHoursTotal(totalSalary.getPayment(), false));
    }
}
