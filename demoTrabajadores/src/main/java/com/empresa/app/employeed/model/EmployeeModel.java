package com.empresa.app.employeed.model;


import lombok.Getter;

import java.time.LocalDate;

@Getter
public class EmployeeModel {
    private Long genderId;
    private Long jobId;
    private String name;
    private String lastName;
    private LocalDate birthDate;

    public EmployeeModel(Long genderId, Long jobId, String name, String lastName, LocalDate birthDate) {
        this.genderId = genderId;
        this.jobId = jobId;
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }
}
