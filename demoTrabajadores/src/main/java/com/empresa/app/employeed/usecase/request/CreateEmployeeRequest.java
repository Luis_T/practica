package com.empresa.app.employeed.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
public class CreateEmployeeRequest {
    private Long genderId;
    private Long jobId;
    private String name;
    private String lastName;
    private LocalDate birthDate;
}
