package com.empresa.app.employeed.repository;

import com.empresa.app.employeed.model.EmployeeModel;
import com.empresa.app.employeed.model.SearcherEmployeeResponse;
import com.empresa.app.employeed.repositoryentity.EmployeeRepositoryEntity;
import com.empresa.app.entities.Employees;
import com.empresa.app.gender.model.SearcherGenderResponse;
import com.empresa.app.job.model.SearcherJobResponse;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository{

    private final EmployeeRepositoryEntity repositoryEntity;

    public EmployeeRepositoryImpl(EmployeeRepositoryEntity repositoryEntity) {
        this.repositoryEntity = repositoryEntity;
    }

    @Override
    public Boolean existName(String name) {
        return this.repositoryEntity.findByName(name).isPresent();
    }

    @Override
    public Boolean validLastName(String lastName) {
        return this.repositoryEntity.findByLastName(lastName).isPresent();
    }

    @Override
    public Long save(EmployeeModel model) {
        Employees entity = this.repositoryEntity.save(this.mapEmployee(model));
        return entity.getId();
    }

    @Override
    public List<SearcherEmployeeResponse> findAllEmployees(){
        List<Employees> employees = this.repositoryEntity.findAll();

        return mapEmployees(employees);
    }

    @Override
    public Boolean existEmployeed(Long employeedId) {
        return this.repositoryEntity.findById(employeedId).isPresent();
    }

    private Employees mapEmployee(EmployeeModel model){
        Employees entity = new Employees();
        entity.setGenderId(model.getGenderId());
        entity.setJobId(model.getJobId());
        entity.setName(model.getName());
        entity.setLastName(model.getLastName());
        entity.setBirthDate(model.getBirthDate());
        return entity;
    }
    private List<SearcherEmployeeResponse> mapEmployees(Collection<Employees> employees){
        return employees.stream().map(e -> {
            return new SearcherEmployeeResponse(
                    e.getId(),
                    e.getName(),
                    e.getLastName(),
                    e.getBirthDate(),
                    new SearcherJobResponse(
                            e.getJobsId().getId(),
                            e.getJobsId().getName(),
                            e.getJobsId().getSalary()
                    ),
                    new SearcherGenderResponse(
                            e.getGendersId().getId(),
                            e.getGendersId().getName()
                    )
            );
        }).collect(Collectors.toList());
    }
}
