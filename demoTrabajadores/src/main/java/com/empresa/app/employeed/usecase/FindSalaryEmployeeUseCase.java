package com.empresa.app.employeed.usecase;

import com.empresa.app.employeed.model.FindSalaryEmployeeModel;
import com.empresa.app.employeed.repository.EmployeeRepository;
import com.empresa.app.employeed.usecase.request.FindSalaryEmployeeRequest;
import com.empresa.app.employeedworkedhours.repository.EmployeedWorkedHoursRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FindSalaryEmployeeUseCase {

    private final EmployeeRepository employeeRepository;

    private final EmployeedWorkedHoursRepository employeedWorkedHoursRepository;

    public FindSalaryEmployeeUseCase(EmployeeRepository employeeRepository, EmployeedWorkedHoursRepository employeedWorkedHoursRepository) {
        this.employeeRepository = employeeRepository;
        this.employeedWorkedHoursRepository = employeedWorkedHoursRepository;
    }

    public FindSalaryEmployeeModel findSalaryEmployee(FindSalaryEmployeeRequest request){
        Double payment = null;
        List<String> errores = new ArrayList<>();

        if (!employeeRepository.existEmployeed(request.getEmployeedId()))
            errores.add("El empleado con id: "+request.getEmployeedId()+" no existe");

        if (!request.getStartDate().isBefore(request.getFinalDate()))
            errores.add("La fecha de inicio no debe ser mayor a la fecha final");

        if (errores.size() == 0)
            payment = this.employeedWorkedHoursRepository.findSalaryEmployee(request.getEmployeedId(),request.getStartDate(),request.getFinalDate());
        return new FindSalaryEmployeeModel(payment, errores);
    }
}
