package com.empresa.app.employeed.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class CreateEmployeeResponse {
    private Long id;

    private List<String> errores;
}
