package com.empresa.app.employeed.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
public class FindSalaryEmployeeRequest {
    private Long employeedId;
    private LocalDate startDate;
    private LocalDate finalDate;
}
