package com.empresa.app.employeed.model;

import com.empresa.app.gender.model.SearcherGenderResponse;
import com.empresa.app.job.model.SearcherJobResponse;
import lombok.Getter;

import java.time.LocalDate;

@Getter
public class SearcherEmployeeResponse {
    private Long id;
    private String name;
    private String lastName;
    private LocalDate birthDate;

    private SearcherJobResponse job;
    private SearcherGenderResponse gender;

    public SearcherEmployeeResponse(Long id, String name, String lastName, LocalDate birthDate, SearcherJobResponse job, SearcherGenderResponse gender) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.job = job;
        this.gender = gender;
    }
}
