package com.empresa.app.employeed.controller;

import com.empresa.app.employeed.model.SearcherEmployeeResponse;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class ResponseEmployees {
    private Boolean success;
    private List<SearcherEmployeeResponse> employees;
    private List<String> errores;
}
