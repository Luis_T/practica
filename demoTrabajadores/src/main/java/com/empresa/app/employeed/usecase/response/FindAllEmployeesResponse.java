package com.empresa.app.employeed.usecase.response;

import com.empresa.app.employeed.model.SearcherEmployeeResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class FindAllEmployeesResponse {
    private List<String> errores;
    private List<SearcherEmployeeResponse> employees;
}
