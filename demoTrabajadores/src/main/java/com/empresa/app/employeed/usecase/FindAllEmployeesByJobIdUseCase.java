package com.empresa.app.employeed.usecase;

import com.empresa.app.employeed.model.SearcherEmployeeResponse;
import com.empresa.app.employeed.repository.EmployeeRepository;
import com.empresa.app.employeed.usecase.request.FindAllEmployeesByJobIdRequest;
import com.empresa.app.employeed.usecase.response.FindAllEmployeesResponse;
import com.empresa.app.entities.Employees;
import com.empresa.app.job.repository.JobRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FindAllEmployeesByJobIdUseCase {
    private final EmployeeRepository employeeRepository;

    private final JobRepository jobRepository;

    public FindAllEmployeesByJobIdUseCase(EmployeeRepository employeeRepository, JobRepository jobRepository) {
        this.employeeRepository = employeeRepository;
        this.jobRepository = jobRepository;
    }

    public FindAllEmployeesResponse execute(FindAllEmployeesByJobIdRequest compareJobId){
        List<String> errores = new ArrayList<>();
        List<SearcherEmployeeResponse> searcher = new ArrayList<>();

        if (!this.jobRepository.exitsJob(compareJobId.getId()))
            errores.add("El puesto de trabajo con id "+compareJobId.getId()+" no existe.");

        if (errores.size() == 0)
            searcher = this.employeeRepository.findAllEmployees().stream().filter(employees -> employees.getJob().getId().equals(compareJobId.getId())).sorted(Comparator.comparing(SearcherEmployeeResponse::getLastName)).collect(Collectors.toList());

        return new FindAllEmployeesResponse(errores, searcher);
    }
}
