package com.empresa.app.job.repositoryentity;

import com.empresa.app.entities.Job;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobRepositoryEntity extends JpaRepository<Job, Long> {
}
