package com.empresa.app.job.controller;

import com.empresa.app.employeed.controller.ResponseEmployees;
import com.empresa.app.employeed.usecase.FindAllEmployeesByJobIdUseCase;
import com.empresa.app.employeed.usecase.request.FindAllEmployeesByJobIdRequest;
import com.empresa.app.employeed.usecase.response.FindAllEmployeesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JobController {
    @Autowired
    private FindAllEmployeesByJobIdUseCase useCase;

    @GetMapping("/jobs/{jobId}/employees")
    @ResponseBody
    public ResponseEntity<?> findAllEmployeesByJobId(@PathVariable("jobId") Long jobId){
        FindAllEmployeesResponse employees = this.useCase.execute(new FindAllEmployeesByJobIdRequest(jobId));
        if (employees.getErrores().size() == 0)
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ResponseEmployees(false, employees.getEmployees(), employees.getErrores()));
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new ResponseEmployees(true, employees.getEmployees(), employees.getErrores() ));
    }
}
