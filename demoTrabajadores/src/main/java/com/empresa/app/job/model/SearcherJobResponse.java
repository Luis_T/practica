package com.empresa.app.job.model;

import lombok.Getter;

@Getter
public class SearcherJobResponse {
    private Long id;
    private String name;
    private Double salary;

    public SearcherJobResponse(Long id, String name, Double salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }
}
