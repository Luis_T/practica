package com.empresa.app.job.repository;

import com.empresa.app.job.repositoryentity.JobRepositoryEntity;
import org.springframework.stereotype.Repository;

@Repository
public class JobRepositoryImpl implements JobRepository{

    private final JobRepositoryEntity jobEntity;

    public JobRepositoryImpl(JobRepositoryEntity jobEntity) {
        this.jobEntity = jobEntity;
    }

    @Override
    public Boolean exitsJob(Long id) {
        return this.jobEntity.existsById(id);
    }
}
